" Configurações básicas "
set nocompatible
set encoding=utf8
set number
set ruler
set showcmd
set laststatus=2 "Mostra a statusline em todas as janelas"
set showtabline=1
set showmode
set t_Co=256

"Backspace apaga identações, quebra de linhas e o caracter onde o Insert Mode
"foi iniciado"
set backspace=indent,eol,start

"Não cria arquivos de backup (arquivos com ~) permanentes, apenas os arquivos
"de proteção contre falhas de salvamento"
set nobackup
set writebackup

"História de comandos e buscas."
set history=200

"Configuração de identação"
set autoindent
set smartindent
"Detecta o tipo do arquivo e ativa o(s) plugin(s) correspondentes e a
"indentação apropriada"
filetype plugin indent on

"Configuração de buscas"
set ignorecase
set smartcase
set incsearch

" Substitui tabs por 4 espaços "
set tabstop=4 shiftwidth=4
set expandtab


syntax enable
"Define o método de 'folding' de acordo com o syntax highlighting"
set foldmethod=syntax

" Abrir linha abaixo e acima sem sair do Normal Mode"
nmap oo o<ESC>k
nmap OO O<ESC>j

" Configuração de cursor em modo bloco quando nos modos Normal, Visual ou Comando
let &t_ti.="\e[1 q"
let &t_SI.="\e[5 q"
let &t_EI.="\e[1 q"
let &t_te.="\e[0 q"

"Esconde a barra de tarefas e de menu no modo gráfico"
set guioptions-=T
set guioptions-=m

"Colore sutilmente a coluna 80 caso esta função esteja disponível (Vim 7.3+)"
if exists('+colorcolumn')
    set colorcolumn=80
endif

set visualbell
